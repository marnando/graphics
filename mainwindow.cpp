#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawscene.h"
#include "netgraphicview.h"

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QDebug>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_graphicView = new NetGraphicView(this);
    centralWidget()->layout()->addWidget(m_graphicView);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_m_drag_clicked()
{
    m_graphicView->setDragMode(QGraphicsView::ScrollHandDrag);
}

void MainWindow::on_m_select_clicked()
{
    m_graphicView->setDragMode(QGraphicsView::NoDrag);
}

void MainWindow::on_m_undo_clicked()
{
}

void MainWindow::on_m_redo_clicked()
{
}

void MainWindow::on_m_zoom_clicked()
{
    m_graphicView->setDragMode(QGraphicsView::RubberBandDrag);
}

void MainWindow::on_m_save_clicked()
{
}
