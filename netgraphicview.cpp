#include "netgraphicview.h"
#include "ui_netgraphicview.h"
#include "drawscene.h"

#include <QDesktopWidget>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>

NetGraphicView::NetGraphicView(QWidget *parent) :
    QGraphicsView(parent),
    ui(new Ui::NetGraphicView)
{
    ui->setupUi(this);

    m_scene = new DrawScene();
    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setScene(m_scene);
    setBackgroundBrush(QColor("white"));

    setMaximumHeight(qApp->desktop()->height() * 8.0);
    setMaximumWidth(qApp->desktop()->width() * 6.0);
}

NetGraphicView::~NetGraphicView()
{
    if (m_scene)
    {
        delete m_scene;
        m_scene = NULL;
    }

    //Deletando itens da rede
    for (int pos = 0; pos < m_listElements.size(); pos++) {
        if (m_listElements[pos]) {
            delete m_listElements[pos];
            m_listElements[pos] = NULL;
        }
    }

    delete ui;
}

void NetGraphicView::onAddNode(QPointF a_point)
{
    QGraphicsEllipseItem * item = new QGraphicsEllipseItem(a_point.x(), a_point.y(), 10, 10);
    item->setBrush(QBrush(Qt::blue));
    item->setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable);
    scene()->addItem(item);
    m_listElements << item;

    if (m_listElements.size() == 2)
    {
        //A posição dos elementos estão em zero, erro!!


        qreal x1 = m_listElements[0]->pos().x();
        qreal y1 = m_listElements[0]->pos().y();

        qreal x2 = m_listElements[1]->pos().x();
        qreal y2 = m_listElements[1]->pos().y();

        addLine(x1, y1, x2, y2);
    }

    setDragMode(QGraphicsView::RubberBandDrag);
}

void NetGraphicView::addLine(qreal y1, qreal x1, qreal x2, qreal y2)
{
    scene()->addLine(x1, y1, x2, y2);
//    scene()->addLine(10.5, 15.5, 50.6, 60.9);
}

void NetGraphicView::releaseListElements()
{

}

void NetGraphicView::mousePressEvent(QMouseEvent *a_event)
{
    if (a_event->button() == Qt::LeftButton)
    {
        onAddNode(mapToScene(a_event->pos()));
    }
}

void NetGraphicView::mouseMoveEvent(QGraphicsSceneMouseEvent *a_event)
{
    Q_UNUSED(a_event)
}

void NetGraphicView::resizeEvent(QResizeEvent *)
{
    m_scene->setSceneRect(0, 0, (qreal)width() - 2.0, (qreal)height() - 2.0);
}

