#ifndef DRAWSCENE_H
#define DRAWSCENE_H

#include <QGraphicsScene>
#include <QMouseEvent>

class DrawScene : public QGraphicsScene
{
    Q_OBJECT

public:
    DrawScene();
    ~DrawScene();

protected:
//    void mousePressEvent(QMouseEvent * a_event);

//    bool eventFilter(QObject *watched, QEvent *event);

signals:
    void signal_addNode(QPoint);

};

#endif // DRAWSCENE_H
