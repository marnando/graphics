#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drawscene.h"
#include "netgraphicview.h"

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QDebug>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_graphicView = new NetGraphicView(this);
    centralWidget()->layout()->addWidget(m_graphicView);

    gerarConnects();

    setWindowTitle(tr("Canvas"));

    installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::gerarConnects()
{
    connect(this, SIGNAL(signal_dragClicked(uint))
            , m_graphicView, SLOT(onManagementViewDragMode(uint)));
}

bool MainWindow::eventFilter(QObject *a_object, QEvent *a_event)
{
    a_event->ignore();

    if (a_object->objectName().compare(m_graphicView->objectName()) == 0) {
        qDebug() << "Classe graphics";
    }

    return QWidget::eventFilter(a_object, a_event);
}

void MainWindow::on_m_drag_clicked()
{
    emit signal_dragClicked(QGraphicsView::ScrollHandDrag);
}

void MainWindow::on_m_select_clicked()
{
    emit signal_dragClicked(QGraphicsView::NoDrag);
}

void MainWindow::on_m_undo_clicked()
{
    emit signal_dragClicked(0);
}

void MainWindow::on_m_redo_clicked()
{
    emit signal_dragClicked(0);
}

void MainWindow::on_m_zoom_clicked()
{
    emit signal_dragClicked(QGraphicsView::RubberBandDrag);
}

void MainWindow::on_m_save_clicked()
{
}
