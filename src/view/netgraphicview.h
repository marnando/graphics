#ifndef NETGRAPHICVIEW_H
#define NETGRAPHICVIEW_H

#include <QWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QAbstractGraphicsShapeItem>

namespace Ui {
class NetGraphicView;
}

class DrawScene;
class NetGraphicView : public QGraphicsView
{
    Q_OBJECT

public:
    DrawScene * m_scene;
    QList<QPair<QGraphicsItem *, QPointF> > m_listElements;

public:
    explicit NetGraphicView(QWidget *parent = 0);
    ~NetGraphicView();

    void onAddNode(QPointF a_point);


    void addLine(qreal y1, qreal x1, qreal x2, qreal y2);
protected:
    void releaseListElements();

    bool event(QEvent * a_event);
    void mousePressEvent(QMouseEvent * a_event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * a_event);
    void resizeEvent(QResizeEvent *);

public slots:
    void onManagementViewDragMode(unsigned int a_graphicManagement);

private:
    Ui::NetGraphicView *ui;
};

#endif // NETGRAPHICVIEW_H
