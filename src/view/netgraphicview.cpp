#include "netgraphicview.h"
#include "ui_netgraphicview.h"
#include "drawscene.h"

#include <QDesktopWidget>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>
#include <QDebug>

NetGraphicView::NetGraphicView(QWidget *parent) :
    QGraphicsView(parent),
    m_listElements(QList<QPair<QGraphicsItem *, QPointF> >()),
    ui(new Ui::NetGraphicView)
{
    ui->setupUi(this);

    m_scene = new DrawScene();
    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setScene(m_scene);
    setBackgroundBrush(QColor("white"));

    setMaximumHeight(qApp->desktop()->height());
    setMaximumWidth(qApp->desktop()->width());

    installEventFilter(this);
}

NetGraphicView::~NetGraphicView()
{
    if (m_scene)
    {
        delete m_scene;
        m_scene = NULL;
    }

    delete ui;
}

void NetGraphicView::onAddNode(QPointF a_point)
{
    QGraphicsEllipseItem * item = new QGraphicsEllipseItem(a_point.x(), a_point.y(), 10, 10);
    item->setBrush(QBrush(Qt::blue));
    item->setFlags(QGraphicsItem::ItemIsSelectable
                 | QGraphicsItem::ItemIsMovable);
    scene()->addItem(item);
    m_listElements << QPair<QGraphicsItem *, QPointF>(item, a_point);
}

void NetGraphicView::addLine(qreal y1, qreal x1, qreal x2, qreal y2)
{
    scene()->addLine(x1, y1, x2, y2);
}

void NetGraphicView::releaseListElements()
{
    if (m_listElements.size() > 0) {
        for (int pos = 0; pos < m_listElements.size(); pos++) {
            if (m_listElements[pos].first) {
                delete m_listElements[pos].first;
            }
        }
         m_listElements.clear();
    }
}

bool NetGraphicView::event(QEvent *a_event)
{
    if (a_event->type() == QEvent::MouseButtonPress) {
        qDebug() << "Acessou mousePressEvent";
        onAddNode(mapToScene(QCursor::pos()));
    }

    return QGraphicsView::event(a_event);
}

void NetGraphicView::mousePressEvent(QMouseEvent *a_event)
{
    qDebug() << "Acessou mousePressEvent";

    if (a_event->button() == Qt::LeftButton)
    {
        onAddNode(mapToScene(a_event->pos()));
    } else
    if (a_event->button() == Qt::RightButton)
    {
//        QList<QGraphicsItem*> list = scene()->items();
//        foreach(QGraphicsItem* item, list){
//            item->setSelected(false);
//            qDebug()<<"selection: "<<item->isSelected();
//        }

//        foreach(QGraphicsItem* item, list){
//            qDebug()<<"selection: "<< item->isSelected();
//        }
    }
}

void NetGraphicView::mouseMoveEvent(QGraphicsSceneMouseEvent *a_event)
{
    Q_UNUSED(a_event)
}

void NetGraphicView::resizeEvent(QResizeEvent *)
{
    m_scene->setSceneRect(0, 0, (qreal)width() - 2.0, (qreal)height() - 2.0);
}

void NetGraphicView::onManagementViewDragMode(unsigned int a_graphicManagement)
{
    switch (a_graphicManagement) {

    case 1:
        setDragMode(QGraphicsView::ScrollHandDrag);
        break;

    case 2:
        setDragMode(QGraphicsView::RubberBandDrag);
        break;

    default:
        setDragMode(QGraphicsView::NoDrag);
        break;
    }

}




















