#ifndef NETGRAPHICVIEW_H
#define NETGRAPHICVIEW_H

#include <QWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QAbstractGraphicsShapeItem>

namespace Ui {
class NetGraphicView;
}

class DrawScene;
class NetGraphicView : public QGraphicsView
{
    Q_OBJECT

public:
    DrawScene * m_scene;
    QList<QGraphicsEllipseItem *> m_listElements;

public:
    explicit NetGraphicView(QWidget *parent = 0);
    ~NetGraphicView();

    void onAddNode(QPointF a_point);


    void addLine(qreal y1, qreal x1, qreal x2, qreal y2);
protected:
    void releaseListElements();

    void mousePressEvent(QMouseEvent * a_event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * a_event);
    void resizeEvent(QResizeEvent *);


private:
    Ui::NetGraphicView *ui;
};

#endif // NETGRAPHICVIEW_H


/*
Exemplo matemático do point

#include <QtMath>

QPointF mapToScene(const QPointF &a_point, qreal a_pressure = 0.0);
QPointF mapToScene(const QPoint &a_point);


QPointF NetGraphicView::mapToScene(const QPointF &a_point, qreal a_pressure)
{
    Q_UNUSED(a_pressure)

    double tmp;

    qreal xf = qAbs(modf(a_point.x(), &tmp));
    qreal yf = qAbs(modf(a_point.y(), &tmp));

    QPoint p0(floor(a_point.x()), floor(a_point.y()));
    QPointF p1 = mapToScene(p0);
    QPointF p2 = mapToScene(p0 + QPoint(1,1));

    QPointF mapped((p1.x() - p2.x()) * xf + p2.x()
                 , (p1.y() - p2.y()) * yf + p2.y());

    return mapped;
}

QPointF NetGraphicView::mapToScene(const QPoint &a_point)
{
    return QGraphicsView::mapToScene(a_point);
}

 */
