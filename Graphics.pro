#-------------------------------------------------
#
# Project created by QtCreator 2014-07-17T12:24:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Graphics
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    drawscene.cpp \
    netgraphicview.cpp

HEADERS  += mainwindow.h \
    drawscene.h \
    netgraphicview.h

FORMS    += mainwindow.ui \
    netgraphicview.ui
