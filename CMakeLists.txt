cmake_minimum_required(VERSION 2.8)
project(canvas)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS_DEBUG} ${Qt5Core_EXECUTABLE_COMPILE_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS} -Wall -std=c++0x")
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

# FILES
file(GLOB
    SRC_LIST
    *.cpp
    src/view/*.cpp
    src/control/*.cpp
    src/model/*.cpp
)

file(GLOB
    UI_LIST
    src/forms/*.ui
)

file(GLOB
    U_H_LIST
    ui_*.h
)

file(GLOB
    RSC_LIST
    *.rsc
    css/*.qss
    css/*.css
    images/*.png
    translations/*.ts
)

add_executable( ${PROJECT_NAME} WIN32 ${SRC_LIST} ${RSC_LIST} ${UI_LIST} )

find_package( Qt5Widgets )
find_package( Qt5Core )

qt5_use_modules( ${PROJECT_NAME} Widgets Core )
qt5_wrap_ui( ${U_H_LIST} ${UI_LIST} )
