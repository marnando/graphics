#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsItem>

namespace Ui {
class MainWindow;
}

class NetGraphicView;
class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    NetGraphicView * m_graphicView;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:

public slots:

private slots:
    void on_m_drag_clicked();
    void on_m_select_clicked();
    void on_m_undo_clicked();
    void on_m_redo_clicked();
    void on_m_zoom_clicked();
    void on_m_save_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
